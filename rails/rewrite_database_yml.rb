#!/usr/bin/env ruby

require "yaml"

db = YAML::load_file("config/database.yml")

production = db["production"]
if production == nil:
  production = {}
  db["production"] = production
end
production["adapter"] = "mysql2"
production["host"]  = ENV["DEPLOYFU_MYSQL_HOST"]
production["database"]  = ENV["DEPLOYFU_MYSQL_DATABASE"]
production["username"]  = ENV["DEPLOYFU_MYSQL_USER"]
production["password"]  = ENV["DEPLOYFU_MYSQL_PASSWORD"]

File.open("config/database.yml", 'w') {|f| f.write(YAML::dump(db)) }
