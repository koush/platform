from ConfigParser import ConfigParser
import os

config = ConfigParser()
config.read("deployfu.ini")
config.set("server:main", "host", "0.0.0.0")
config.set("server:main", "port", os.environ["PORT"])

app = None
for section in config.sections():
    if section.startswith("app:"):
        app = section

if app is not None:
    config.set(app, "sqlalchemy.url", "mysql://%(user)s:%(password)s@%(host)s/%(database)s" % { 'user': os.getenv("DEPLOYFU_MYSQL_USER"), 'password': os.getenv("DEPLOYFU_MYSQL_PASSWORD"), 'host': os.getenv("DEPLOYFU_MYSQL_HOST"), 'database': os.getenv("DEPLOYFU_MYSQL_DATABASE") } )

with open('deployfu.ini', 'wb') as configfile:
    config.write(configfile)

